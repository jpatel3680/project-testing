<?php

// Includs database connection
include "config.php";

try {
	$connection = new PDO ($dsn);
	
	$sql = "SELECT * FROM Product";
	
	$statement = $connection->prepare ( $sql );
	$statement->execute ();
	
	$result = $statement->fetchAll (PDO::FETCH_ASSOC);
} 
catch ( PDOException $error ) {
	echo $sql . "<br>" . $error->getMessage ();
}

?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Product Management Company</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
	<!-- Animate CSS  -->
    <link rel="stylesheet" type="text/css" href="css/animate.css" media="screen">
	<style>
		.error {color: #FF0000;}
	</style>

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="index.php">Product Management Company</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

      <!-- Navbar Search -->
      <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
          <div class="input-group-append">
            <button class="btn btn-primary" type="button">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Navbar -->
      <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <span class="badge badge-danger"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
          <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          
			<i class="fas fa-envelope fa-fw"></i>
            
			
			<span class="badge badge-danger"></span>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown no-arrow">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-user-circle fa-fw"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="#">Settings</a>
            <a class="dropdown-item" href="#">Activity Log</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
          </div>
        </li>
      </ul>

    </nav>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header">Login Screens:</h6>
            <a class="dropdown-item" href="login.html">Login</a>
            <a class="dropdown-item" href="register.html">Register</a>
            <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">Other Pages:</h6>
            <a class="dropdown-item" href="404.html">404 Page</a>
            <a class="dropdown-item" href="blank.html">Blank Page</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>

          <!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <label style="float:left;padding-top:10px;"><i class="fas fa-table"></i>
              Product Table</label>
			  <!-- Button to trigger modal -->
					<?php 
					if(isset($_POST['TotalCost'])){
						try {
							$connection = new PDO ($dsn);
							
							$sql = "SELECT SUM(Cost) FROM Coverage"; 
							$result = $connection->prepare($sql); 
							$result->execute(); 
							$number_of_columns = $result->fetchColumn(); 
							echo "<script type='text/javascript'>alert('Total Cost : $'+$number_of_columns);</script>";
						} 
						catch ( PDOException $error ) {
							echo $sql . "<br>" . $error->getMessage ();
						}
					}
					?>
					
					
					<button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalForm" style="float:right;">
						ADD PRODUCT
					</button>
						
					
					<?php
					
						if(isset($_POST['ID']) &&  isset($_POST['PRODUCT_NAME'])&&  isset($_POST['COST']))
						{
									$ID = $_POST['ID'];
		$PRODUCT_NAME = $_POST ['PRODUCT_NAME'];
		$COST = $_POST ['COST'];
		
		$sql = "INSERT INTO Product (ID,PRODUCT_NAME,COST) VALUES (:ID,:PRODUCT_NAME,:COST)";
// 		echo "<h3>SQL:" . $sql . "</h3>";
		$statement = $connection->prepare ( $sql );
		$statement->bindParam ( ':ID', $ID);
		$statement->bindParam ( ':PRODUCT_NAME', $PRODUCT_NAME);
		$statement->bindParam ( ':COST', $COST);
		
		
									
									if($statement->execute()){
									
										
										echo "<script type='text/javascript'>alert('Data Added successfully...'); </script>";
										
										$to = "jainupatel95@gmail.com";
										$subject = "Product Added";
										$message = "Product Added Successfully!!";
										$header = "From:products@company.com \r\n";
										$retval = mail($to, $subject, $message, $header);
										if ($retval == true) {
											echo "Data added successfully...";
										} else {
											echo "Data not added...";
										}
									}
									
									else
									{
										echo "<script type='text/javascript'>alert('Data could not be added...'); </script>";
									}
									
						}
						
						$sql = "SELECT * FROM Product";
						$statement = $connection-> prepare ( $sql );
		
						$statement->execute ();
		
						$result = $statement->fetchAll ();
					
					?>
				
					<!-- Modal -->
					<div class="modal fade" id="modalForm" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<!-- Modal Header -->
								<div class="modal-header">
								<h4 class="modal-title" id="myModalLabel">ADD PRODUCT DETAILS</h4>
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">&times;</span>
										<span class="sr-only">Close</span>
									</button>
									
								</div>
								<script>
								
		function validateForm() {
			var y = document.form.PRODUCT_NAME.length;
			if (y < 3 && y >25) {
    alert("PLEASE ENTER PRODUCT NAME BETWEEN 3 TO 25 CHARACTERS");
    return false;
}

			var x = document.forms["form"]["PRODUCT_NAME"].value;
			if (x !=< 3 && y >25 ) {
				alert("PLEASE ENTER PRODUCT NAME BETWEEN 3 TO 25 CHARACTERS");
				return false;
			}
			
			
		}
		
		</script>

								
								}
</script>
								<!-- Modal Body -->
								<div class="modal-body">
									<p class="statusMsg"></p>
									<form role="form" name="form" onsubmit="return validateForm()" method="POST" id="contactForm" class="contact-form" data-toggle="validator" class="shake">
										<!--<p><span class="error">* required field</span></p>-->
										<div class="form-group">
											<label for="inputName">PRODUCT ID</label>
											<input type="text" class="form-control" name="ID" id="ID" placeholder="Enter Product id" required data-error="Please enter Product id"/>
											<div class="help-block with-errors" style="color:red;"></div>
										</div>
										<div class="form-group">
											<label for="inputName">PRODUCT NAME</label>
											<input type="text" class="form-control" name="PRODUCT_NAME" id="PRODUCT_NAME" placeholder="Enter your Product Name" required data-error="Please enter Product Name"/>
											<div class="help-block with-errors" style="color:red;"></div>
										</div>
										<div class="form-group">
											<label for="inputCost">COST</label>
											<input type="text" class="form-control" name="COST" id="COST" placeholder="Enter your Cost" required data-error="Please enter your Cost"/>
											<div class="help-block with-errors" style="color:red;"></div>
										</div>
										<!-- Modal Footer -->
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="reset" class="btn btn-default">Reset</button>
											<button type="submit" class="btn btn-success" >SUBMIT</button>
										</div>
										<div id="msgSubmit" class="h3 text-center hidden"></div> 
										<div class="clearfix"></div>   
									</form>
								</div>
								
								
							</div>
						</div>
					</div>
					
					
				<!-- Main JS  -->
				<!--<script type="text/javascript" src="js/jquery-min.js"></script>  -->    
				<script type="text/javascript" src="js/form-validator.min.js"></script>  
				<script type="text/javascript" src="js/contact-form-script.js"></script>
				
				

				
				
			  </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
					<th width="20%">ID</th>
                      <th width="20%">PRODUCT_NAME</th>
                      <th width="20%">COST</th>
					  <th width="10%">Update</th>
					  <th width="10%">Delete</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
					<th width="20%">ID</th>
                      <th width="20%">PRODUCT_NAME</th>
                      
                      <th width="20%">Cost</th>
					  <th width="10%">Update</th>
					  <th width="10%">Delete</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    
                    <?php foreach ( $result as $row )  {?>
					<tr>
					<td><?php echo $row['ID'];?></td>
                      <td><?php echo $row['PRODUCT_NAME'];?></td>
					  <td><?php echo $row['COST'];?></td>
                      <td>
						<a class="btn btn-primary submitBtn"  href="UpdateFile.php?ID=<?php echo $row['ID'];?>">Edit</a> </td> 
						
						<td><a class="btn btn-primary submitBtn" href="deleteFile.php?ID=<?php echo $row['ID'];?>" onclick="return confirm('Are you sure?');">Delete</a>
					  </td>
					   
						
					  
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright ©Product Management Company Website 2018</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>
	
	
	


  </body>

</html>
